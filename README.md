# CakePHP Sentry ErrorLogger

Since CakePHP 4.4, the error handlers have been updated to `ErrorTrap` and `ExceptionTrap`.
Both use `Cake\Error\ErrorLoggerInterface`, making packages such as `connehito/CakeSentry` less interesting.

This repository provides a Sentry implementation of the `Cake\Error\ErrorLoggerInterface` 

## Configure

In your `config/bootstrap.php`
```php
/*
 * Register application error and exception handlers.
 */
$errorConfig = Cake\Core\Configure::consume('Error');

//if sentry, log to SentryErrorLogger
if (Configure::read('Sentry.dsn', false)) {
	$errorConfig = [
		...$errorConfig,
		'logger' => SentryErrorLogger::class
	];
}
(new ErrorTrap($errorConfig))->register();
(new ExceptionTrap($errorConfig))->register();
```

In your `config/app.php`

you can either use the `Sentry` key
```php
[
    'Sentry' => [
        'dsn' => '......'        
    ] 
```


or `Error.Sentry`
```php
[
    /*...*/
    'Error' => [
        /*....*/
        'Sentry' => [
            'dsn' => '......'        
        ]   
    ]
```

## Setting options
Global
```

```

Per Error/Exception
```

```