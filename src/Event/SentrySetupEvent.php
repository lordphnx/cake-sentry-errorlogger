<?php

namespace Lordphnx\CakeSentryErrorlogger\Event;

use Sentry\Client;
use Cake\Event\Event;

/**
 * @extends Event<Client>
 */
class SentrySetupEvent extends Event {
	public const key = 'CakeSentry.Client.afterSetup';

	public function __construct(Client $client) {
		parent::__construct(self::key, $client, null);
	}

}