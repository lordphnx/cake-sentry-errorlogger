<?php

namespace Lordphnx\CakeSentryErrorlogger\Event;

use Cake\Event\Event;
use Sentry\Client;
use Sentry\State\Scope;

/**
 * @extends Event<Scope>
 */
class SentryAfterCaptureEvent extends Event {
	public static string $key = 'CakeSentry.Client.afterCapture';

    /**
     * @todo: check what data this should receive and where we should throw it?
     * @param Scope $scope
     * @param \Exception $exception
     * @param ServerRequestInterface|null $request
     */
	public function __construct(Scope $scope, \Throwable $exception, ServerRequestInterface|null $request = null) {
		parent::__construct(self::$key, $scope, ['exception' => $exception, 'request' => $request]);
	}

}