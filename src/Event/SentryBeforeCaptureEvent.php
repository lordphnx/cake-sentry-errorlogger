<?php

namespace Lordphnx\CakeSentryErrorlogger\Event;

use Cake\Event\Event;
use Psr\Http\Message\ServerRequestInterface;
use Sentry\Client;
use Sentry\State\Scope;

/**
 * @extends Event<Scope>
 */
class SentryBeforeCaptureEvent extends Event {
	public static string $key = 'CakeSentry.Client.beforeCapture';

	public function __construct(Scope $scope, \Throwable $exception, ServerRequestInterface|null $request = null) {
		parent::__construct(self::$key, $scope, ['exception' => $exception, 'request' => $request]);
	}

}