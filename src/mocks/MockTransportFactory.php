<?php

namespace Lordphnx\CakeSentryErrorlogger\mocks;

use Sentry\Options;
use Sentry\Transport\TransportFactoryInterface;
use Sentry\Transport\TransportInterface;

class MockTransportFactory implements TransportFactoryInterface
{
    public function create(Options $options): TransportInterface
    {
        return MockTransport::getInstance();

    }


}