<?php

namespace Lordphnx\CakeSentryErrorlogger\mocks;

use GuzzleHttp\Promise\Promise;
use GuzzleHttp\Promise\PromiseInterface;
use Sentry\Event;
use Sentry\Transport\TransportInterface;

class MockTransport implements TransportInterface
{

    public array $sent = [];

    private static ?MockTransport $instance = null;


    public function send(Event $event): PromiseInterface
    {
        $this->sent[] = $event;
        return new Promise();
    }

    public function close(?int $timeout = null): PromiseInterface
    {
        return new Promise();
    }

    public function reset()
    {
        $this->sent = [];
    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new MockTransport();
        }

        return self::$instance;
    }


}