<?php

namespace Lordphnx\CakeSentryErrorlogger\Log;

use Cake\Core\Configure;
use Cake\Core\InstanceConfigTrait;
use Cake\Error\ErrorLogger;
use Cake\Error\ErrorLoggerInterface;
use Cake\Error\PhpError;
use Cake\Event\EventManager;
use Lordphnx\CakeSentryErrorlogger\Event\SentryBeforeCaptureEvent;
use Lordphnx\CakeSentryErrorlogger\Event\SentrySetupEvent;
use Psr\Http\Message\ServerRequestInterface;
use Sentry\Client;
use Sentry\ClientBuilder;
use Sentry\Event;
use Sentry\SentrySdk;
use Sentry\Severity;
use Sentry\Stacktrace;
use Sentry\State\Scope;
use Throwable;

/**
 * An implementation of {@link ErrorLoggerInterface} that logs to Sentry
 * It uses a config provided, or per default tries to read the "Sentry" Config key
 *
 * @property Client $client internal
 *
 */
class SentryErrorLogger extends ErrorLogger
{

    use InstanceConfigTrait;

    /**
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        
        $sentryConfig = [
            ...Configure::read('Sentry', []),
            ...($config['Sentry'] ?? [])
        ];


        if (!empty($sentryConfig['transportFactory'])) {
            $transportFactoryName = $sentryConfig['transportFactory'];
            unset($sentryConfig['transportFactory']);
        }

        $builder = ClientBuilder::create($sentryConfig);

        if (isset($transportFactoryName)) {
            $builder->setTransportFactory(new $transportFactoryName());
        }


        $this->client = $builder->getClient();

        EventManager::instance()->dispatch(new SentrySetupEvent($this->client));
    }

    /**
     * {@inheritDoc}
     */

    public function logException(Throwable $exception, ?ServerRequestInterface $request = null, bool $includeTrace = false): void
    {

        SentrySdk::getCurrentHub()->withScope(function (Scope $scope) use ($exception, $request) {

            $this->addRequestToScope($scope,$request);
            EventManager::instance()->dispatch(new SentryBeforeCaptureEvent($scope, $exception, $request));


            $eventId = $this->client->captureException($exception, $scope);
        });


    }


    public function logError(PhpError $error, ?ServerRequestInterface $request = null, bool $includeTrace = false): void
    {
        SentrySdk::getCurrentHub()->withScope(function (Scope $scope) use ($error, $request, $includeTrace) {

            $this->addRequestToScope($scope,$request);
            

            $message = $error->getMessage();
            if ($request) {
                $message .= $this->getRequestContext($request);
            }

            if ($includeTrace) {
                $message .= "\nTrace:\n" . $error->getTraceAsString() . "\n";
            }

            $logMap = [
                'strict' => LOG_NOTICE,
                'deprecated' => LOG_NOTICE,
            ];
            $level = $error->getLabel();
            $level = $logMap[$level] ?? $level;


            $this->client->captureMessage($message, Severity::error());
        });
    }


    public function addRequestToScope(Scope $scope, ?ServerRequestInterface $request = null): void
    {
        if ($request === null) {
            return;
        }
        $scope->setContext('request', [
            'url' => (string)$request->getUri(),
            'data' => $request->getParsedBody(),
            'cookies' => $request->getCookieParams(),
            'headers' => $request->getHeaders(),
            'method' => $request->getMethod()
            //'env' => $_SERVER is disabled by default but can be added manually using the BeforeCaptureEvent
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function logMessage($level, string $message, array $context = []): bool
    {
        //NOP BY DESIGN
        return true;

    }

}