<?php

namespace Lordphnx\CakeSentryErrorlogger\Test\TestCase;

use Cake\Core\Configure;
use Cake\Error\Debugger;
use Cake\Error\PhpError;
use Cake\Event\EventManager;
use Cake\Http\ServerRequest;
use Cake\TestSuite\TestCase;
use Lordphnx\CakeSentryErrorlogger\Event\SentryBeforeCaptureEvent;
use Lordphnx\CakeSentryErrorlogger\Log\SentryErrorLogger;
use Lordphnx\CakeSentryErrorlogger\mocks\MockTransport;
use Lordphnx\CakeSentryErrorlogger\mocks\MockTransportFactory;
use Sentry\Event;
use Sentry\State\Scope;
use function Sentry\configureScope;

/**
 * @property SentryErrorLogger $client
 */
class SentryErrorLoggerTest extends TestCase
{

    protected function setUp(): void
    {
        parent::setUp();
        EventManager::instance(new EventManager());
        
        $this->client = new SentryErrorLogger([
            'Sentry' => [
                'dsn' => null,
                'environment' => 'production',
                'transportFactory' => MockTransportFactory::class,
            ]
//			'app_version' => '1.2.3',
        ]);

        $sentryClient = $this->client->client;

        MockTransport::getInstance()->reset();
        EventManager::instance()->on(SentryBeforeCaptureEvent::$key, function (SentryBeforeCaptureEvent $event) {
            configureScope(function (Scope $scope) {
                $scope->setTag('app_version', 'PHPUnit-test-case-SentryErrorLoggerTest');
            });
        });

    }


    public function testCaptureError(): void
    {
        self::assertCount(0, MockTransport::getInstance()->sent);

        /**
         * @var SentryErrorLogger $logger
         */
        
        $error = new PhpError(500, 'Test456','MyTestFile.php',1337,
            Debugger::trace(['start' => 1, 'format' => 'points'])
        );

        $req = new ServerRequest([
            'base' => 'https://www.joshmengerink.nl',
            'url' => '/my/second/url'
        ]);

        $this->client->logError($error, $req,true);

        self::assertCount(1, MockTransport::getInstance()->sent);
        /** @var Event $msg */
        [$msg] = MockTransport::getInstance()->sent;
        self::assertStringContainsString('Test456', $msg->getMessage());
    }

    /**
     * Test that capturing exceptions works
     * @return void
     */
    public function testCaptureException(): void
    {
        self::assertCount(0, MockTransport::getInstance()->sent);

        $req = new ServerRequest([
            'base' => 'https://www.joshmengerink.nl',
            'url' => '/my/example/url'
        ]);
        $this->client->logException(new \Exception("Test123"), $req, true);


        self::assertCount(1, MockTransport::getInstance()->sent);
        /** @var Event $msg */
        [$msg] = MockTransport::getInstance()->sent;
        [$ex] = $msg->getExceptions();


        print_r($msg->getRequest());

        self::assertEquals('Test123', $ex->getValue());


    }

}
